<?php

namespace Drupal\Tests\node_title_ps\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * This class provides methods specifically for testing something.
 *
 * @group node_title_ps
 */
class NodeTitlePsFunctionalTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'test_page_test',
    'node',
    'node_title_ps',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->config('system.site')->set('page.front', '/test-page')->save();
    $this->user = $this->drupalCreateUser([]);
    $this->drupalLogin($this->rootUser);
  }

  /**
   * Tests the modules token support.
   */
  public function testTokenSupport() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $this->createContentType(['type' => 'article']);

    // Add suffix and prefix to the node type:
    $this->drupalGet('/admin/structure/types/manage/article');
    $page->fillField('title_prefix', '[node:title] ');
    $page->fillField('title_suffix', ' [node:title]');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);

    // Create a article node:
    $node = $this->createNode([
      'type' => 'article',
      'title' => 'Test',
    ]);

    // Go to the article and check the page heading:
    $this->drupalGet($node->toUrl()->toString());
    $session->statusCodeEquals(200);

    $session->elementTextEquals('css', 'h1', 'Test Test Test');
  }

  /**
   * Tests the prefix / suffix on a node.
   */
  public function testPrefixSuffixOnNode() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $this->createContentType(['type' => 'article']);

    // Add suffix and prefix to the node type:
    $this->drupalGet('/admin/structure/types/manage/article');
    $page->fillField('title_prefix', 'Prefix ');
    $page->fillField('title_suffix', ' Suffix');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);

    // Create a article node:
    $node = $this->createNode([
      'type' => 'article',
      'title' => 'Test',
    ]);

    // Go to the article and check the page heading:
    $this->drupalGet($node->toUrl()->toString());
    $session->statusCodeEquals(200);

    $session->elementTextEquals('css', 'h1', 'Prefix Test Suffix');

    // Now only add a prefix:
    $this->drupalGet('/admin/structure/types/manage/article');
    $page->fillField('title_prefix', 'Prefix ');
    $page->fillField('title_suffix', '');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);

    // Go to the article and check the page heading:
    $this->drupalGet($node->toUrl()->toString());
    $session->statusCodeEquals(200);

    $session->elementTextEquals('css', 'h1', 'Prefix Test');

    // Now only add a suffix:
    $this->drupalGet('/admin/structure/types/manage/article');
    $page->fillField('title_prefix', '');
    $page->fillField('title_suffix', ' Suffix');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);

    // Go to the article and check the page heading:
    $this->drupalGet($node->toUrl()->toString());
    $session->statusCodeEquals(200);

    $session->elementTextEquals('css', 'h1', 'Test Suffix');

    // Now remove both:
    $this->drupalGet('/admin/structure/types/manage/article');
    $page->fillField('title_prefix', '');
    $page->fillField('title_suffix', '');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);

    // Go to the article and check the page heading:
    $this->drupalGet($node->toUrl()->toString());
    $session->statusCodeEquals(200);

    $session->elementTextEquals('css', 'h1', 'Test');
  }

}
