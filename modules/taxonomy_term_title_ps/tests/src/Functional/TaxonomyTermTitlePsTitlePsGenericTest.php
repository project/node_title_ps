<?php

namespace Drupal\Tests\taxonomy_term_title_ps\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module test for taxonomy_term_title_ps.
 *
 * @group taxonomy_term_title_ps
 */
class TaxonomyTermTitlePsTitlePsGenericTest extends GenericModuleTestBase {

  /**
   * {@inheritDoc}
   */
  protected function assertHookHelp(string $module): void {
    // Don't do anything here. Just overwrite this useless method, so we do
    // don't have to implement hook_help().
  }

}
