<?php

namespace Drupal\Tests\taxonomy_term_title_ps\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * This class provides methods specifically for testing something.
 *
 * @group taxonomy_term_title_ps
 */
class TaxonomyTermTitlePsFunctionalTest extends BrowserTestBase {
  use TaxonomyTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'test_page_test',
    'taxonomy',
    'taxonomy_term_title_ps',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->config('system.site')->set('page.front', '/test-page')->save();
    $this->user = $this->drupalCreateUser([]);
    $this->drupalLogin($this->rootUser);
  }

  /**
   * Tests the modules token support.
   */
  public function testTokenSupport() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $vocabularyBundle = $this->createVocabulary(['name' => 'Tags', 'vid' => 'tags']);

    // Add suffix and prefix to the node type:
    $this->drupalGet('/admin/structure/taxonomy/manage/tags');
    $page->fillField('title_prefix', '[term:name] ');
    $page->fillField('title_suffix', ' [term:name]');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);

    // Create a term:
    $term = $this->createTerm($vocabularyBundle, ['name' => 'Test']);

    // Go to the term and check the page heading:
    $this->drupalGet($term->toUrl()->toString());
    $session->statusCodeEquals(200);

    $session->elementTextEquals('css', 'h1', 'Test Test Test');
  }

  /**
   * Tests the prefix / suffix on a taxonomy term.
   */
  public function testPrefixSuffixOnTaxonomyTerm() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $vocabularyBundle = $this->createVocabulary(['name' => 'Tags', 'vid' => 'tags']);

    // Add suffix and prefix to the node type:
    $this->drupalGet('/admin/structure/taxonomy/manage/tags');
    $page->fillField('title_prefix', 'Prefix ');
    $page->fillField('title_suffix', ' Suffix');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);

    // Create a term:
    $term = $this->createTerm($vocabularyBundle, ['name' => 'Test']);

    // Go to the article and check the page heading:
    $this->drupalGet($term->toUrl()->toString());
    $session->statusCodeEquals(200);

    $session->elementTextEquals('css', 'h1', 'Prefix Test Suffix');

    // Now only add a prefix:
    $this->drupalGet('/admin/structure/taxonomy/manage/tags');
    $page->fillField('title_prefix', 'Prefix ');
    $page->fillField('title_suffix', '');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);

    // Go to the article and check the page heading:
    $this->drupalGet($term->toUrl()->toString());
    $session->statusCodeEquals(200);

    $session->elementTextEquals('css', 'h1', 'Prefix Test');

    // Now only add a suffix:
    $this->drupalGet('/admin/structure/taxonomy/manage/tags');
    $page->fillField('title_prefix', '');
    $page->fillField('title_suffix', ' Suffix');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);

    // Go to the article and check the page heading:
    $this->drupalGet($term->toUrl()->toString());
    $session->statusCodeEquals(200);

    $session->elementTextEquals('css', 'h1', 'Test Suffix');

    // Now remove both:
    $this->drupalGet('/admin/structure/taxonomy/manage/tags');
    $page->fillField('title_prefix', '');
    $page->fillField('title_suffix', '');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);

    // Go to the article and check the page heading:
    $this->drupalGet($term->toUrl()->toString());
    $session->statusCodeEquals(200);

    $session->elementTextEquals('css', 'h1', 'Test');
  }

}
